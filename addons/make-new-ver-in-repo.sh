#!/usr/bin/env bash

# MyKodi/RELEASE/kodi_repo/addons

addons_dir=$PWD
kodi_repo_dir=$(dirname $addons_dir)/repo

if [ $# -eq 0 ]; then
    echo "No arguments supplied"
	exit
fi

if [ -z "$1" ]; then
    echo "No repo argument supplied"
	exit
fi

if [ -z "$2" ]; then
    echo "No version argument supplied"
	exit
fi

repo="$1"
ver="$2"

python='/usr/local/bin/python'

echo $repo-$ver

pushd $repo > /dev/null
    pwd

    git reset --hard HEAD
    git pull

    perl -pi -e "s,[\"']${repo}[\"'](.+)version=[\"'](.+?)[\"'],\"${repo}\"\1version=\"${ver}\"," "addon.xml"
    rm *.bak

    #if [ ! -z "$3" ]; then
    #	changes="$3"
    #	../chlogadd.sh $ver "$changes"
    #	git add "changelog.txt"
    #fi

    git add "addon.xml"
    git commit -m "Bump V${ver}"
    git tag "V${ver}"
    git push

popd > /dev/null

#--------------------------------------------------------------------------------
pushd $repo > /dev/null

    mkdir -p "$kodi_repo_dir/${repo}"
    zip_out="$kodi_repo_dir/${repo}/${repo}-${ver}.zip"
    git archive --format=zip -v --output="${zip_out}" HEAD --prefix=${repo}/
    md5sum "${zip_out}" | cut -d' ' -f1 | tr -d '\n' > "${zip_out}.md5"

popd > /dev/null

cp -fv ${repo}/addon.xml "$kodi_repo_dir/${repo}/" 
#--------------------------------------------------------------------------------

pushd $kodi_repo_dir > /dev/null
    pwd

    echo Start generate
    echo $python '@generate.py'
    $python '@generate.py'

    git add addons.*
    git add ${repo}

    git commit -m "$repo-$ver"
    git push

popd > /dev/null

pushd .. > /dev/null
    git add --force -- addons/${repo}
    git commit -m "Submodule up!"
    git push --recurse-submodules=check
popd


