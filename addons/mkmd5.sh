#!/bin/bash

zip_out="$1"
echo ${zip_out}

md5sum "${zip_out}" | cut -d' ' -f1 | tr -d '\n' > "${zip_out}.md5"