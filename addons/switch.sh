#!/bin/bash

path="$HOME/Library/Application Support"
kodi_link="$path/Kodi"
kodi_src="$path/Kodi.leia"

if [[ ! -L "$kodi_link" && -d "$kodi_link" ]]
then
    echo "$kodi_link is a directory, is not a symlink. Not switched"
    exit 1
fi

if [[ -L "$kodi_link" && -d "$kodi_link" ]]
then
    echo "$kodi_link is a symlink to a directory, remove that."

    rm "$kodi_link"
fi

if [ ! -f "$kodi_link" ]; then
    echo "'$kodi_link' does not exist. Create symbolic link"
    ln -s "$kodi_src" "$kodi_link"
fi

ls -l "$path" | grep Kodi

