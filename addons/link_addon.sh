#!/bin/bash

if [ -n "$1" ]
then
    repo=$1
else
    echo 'No addon selected'
    exit 1
fi

./switch.sh

path="$HOME/Library/Application Support"
addons="$path/Kodi/addons"

repo_link="$addons/$repo"
repo_src="$PWD/$repo"

if [[ ! -L "$repo_link" && -d "$repo_link" ]]
then
    # directory, not link
    rm -R "$repo_link"
fi

if [[ -L "$repo_link" && -d "$repo_link" ]]
then
    # link to dir
    rm "$repo_link"
fi

ln -s "$repo_src" "$repo_link"