#!/bin/bash

ver=$1
param=$2
now=$(date +%d-%m-%Y)
pattern='1s/^/'$ver' ('$now')\n'$param'\n\n/'

sed "$pattern" changelog.txt > temp.chlog
mv temp.chlog changelog.txt